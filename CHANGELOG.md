# Changelog
## V2.7
- Modify the cost calculation node for solving the capex in 1990 problem
- optimize some calculation nodes
- Integrate the BECC from Power sector

## V2.6 (before 3.0)
- Add new inputs to TPE
- Change one technology name to TPE and Employment
- Updata some calculations

## 2.1 v2
- Solve the negative cost issue

## 2.1 V1
- Update the cost calculation
- Change the occupation percentage to 200% according to the talk with Bernd in Potsdam

## 1.9 V5
- Add interface between ccus and refinery, storage and refinery respectively
- Make modification of cost calculation

## 1.9 V4
- Remove the interface between ccus and storage

## 1.9 V3
- delete the import from Refinery to avoid loop
- add the output of ng to WP5.3
## 1.9 V2
- delete the output of ng to WP5.3 for avoiding loop

## 1.9 Unreleased
- Update some data
- Delete several technologies
- Build interface between CCUS and Employment, as well as CCUS with Storage 

## 1.8_v3
- Integration of cost

## 1.8_v2
- Update technology efficiencies
- Add ccs potential occupation as output to pathway explore

## 1.7_v3
- Remove the excel writer in node 3.2 CCUS
- Move the excel reader of ccs potential out of the node 3.2 CCUS
- Rename the output port of metanodes

## 1.7_v2
- Update the lever_ccus values
- Deleted ocean_storage, deep-ocean-injection and mineral-storage, and embedded them into existed technologies
- Integrated the ccs potential data for each country
- Added a python node for handling the situation where the ccs potential is met
- Integrated the co2 transport as a new technology in Knime
- Updated the interface between CCUS and WP3.1

## 1.5_v2
- change the excel reader to Knime:
- stage all

## 1.5
- Updated the interface with WP5.1 and WP5.2
- Change technologies codes in line with EUCalc convention
- Add calibration

## 1.4
- Initial version, integrating all modules